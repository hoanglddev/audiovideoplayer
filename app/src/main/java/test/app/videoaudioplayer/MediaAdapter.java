package test.app.videoaudioplayer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface MediaAdapterListener {
        void onItemClick(int position, MediaObject mediaObject);
    }

    private List<MediaObject> mediaObjects;
    private Context context;
    private MediaAdapterListener listener;

    public MediaAdapter(MediaAdapterListener listener) {
        this.mediaObjects = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.media_list_item, parent, false);

        return new MediaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((MediaViewHolder) viewHolder).bindData(position, mediaObjects.get(position));
    }

    @Override
    public int getItemCount() {
        return mediaObjects.size();
    }

    public void setData(List<MediaObject> mediaObjects) {
        this.mediaObjects = mediaObjects;
        notifyDataSetChanged();
    }

    public List<MediaObject> getData() {
        return this.mediaObjects;
    }

    public class MediaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.media_list_item_tv_title)
        TextView tvTitle;

        @BindView(R.id.media_list_item_tv_artist)
        TextView tvArtist;

        @BindView(R.id.media_list_item_im_icon)
        ImageView imIcon;

        private int position;
        private MediaObject mediaObject;

        public MediaViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindData(int position, MediaObject mediaObject) {
            if (mediaObject == null) return;

            this.position = position;
            this.mediaObject = mediaObject;

            tvTitle.setText(mediaObject.getTitle());
            tvArtist.setText(mediaObject.getArtTist());

            if (mediaObject.isAudio()) {
                Glide.with(context).load(R.drawable.ic_music).into(imIcon);
            } else {
                Glide.with(context).load(R.drawable.ic_video).into(imIcon);
            }
        }

        @OnClick(R.id.media_list_item_container)
        public void onItemClick() {
            listener.onItemClick(position, mediaObject);
        }
    }
}
