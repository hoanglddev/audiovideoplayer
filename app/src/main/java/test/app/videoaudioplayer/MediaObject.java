package test.app.videoaudioplayer;

import java.io.Serializable;

public class MediaObject implements Serializable {

    private String title;

    private String data;

    private String displayName;

    private String artTist;

    private boolean isAudio;

    public boolean isAudio() {
        return isAudio;
    }

    public void setAudio(boolean audio) {
        isAudio = audio;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getArtTist() {
        return artTist;
    }

    public void setArtTist(String artTist) {
        this.artTist = artTist;
    }

}
