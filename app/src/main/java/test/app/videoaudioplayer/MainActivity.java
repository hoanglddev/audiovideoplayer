package test.app.videoaudioplayer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements MediaAdapter.MediaAdapterListener {

    @BindView(R.id.main_activity_rv_main)
    RecyclerView rvMain;

//    @BindView(R.id.dialog_media_player_view)
    PlayerView playerViewDialog;

    public final static int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 0;

    private SimpleExoPlayer player;

    private MediaAdapter mediaAdapter;
    private AlertDialog dialogMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ButterKnife.bind(this);

        mediaAdapter = new MediaAdapter(this);
        rvMain.setAdapter(mediaAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isHavePermission()) {
            scanFileInDevice();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isHavePermission() {
        int hasPermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "You need to allow read external storage permission." +
                        "\nIf you disable this permission, You will not able to use app.", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_READ_EXTERNAL_STORAGE);
            }
            return false;
        }

        return true;
    }

    private void initializePlayer(String path) {
        if (playerViewDialog == null || TextUtils.isEmpty(path)) return;

        player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());

        playerViewDialog.setPlayer(player);

        player.setPlayWhenReady(true);
        player.seekTo(0, 0);

        Uri uri = Uri.fromFile(new File(path));
        MediaSource mediaSource = buildMediaSource(uri);

        player.prepare(mediaSource, true, false);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource(uri,
                new DefaultDataSourceFactory(this,"ua"),
                new DefaultExtractorsFactory(), null, null);
    }

//    private MediaSource buildMediaSource2(Uri uri) {
//        // these are reused for both media sources we create below
//        DefaultExtractorsFactory extractorsFactory =
//                new DefaultExtractorsFactory();
//        DefaultHttpDataSourceFactory dataSourceFactory =
//                new DefaultHttpDataSourceFactory( "user-agent");
//
//        ExtractorMediaSource videoSource =
//                new ExtractorMediaSource.Factory(
//                        new DefaultHttpDataSourceFactory("exoplayer-codelab")).
//                        createMediaSource(uri);
//
//        Uri audioUri = Uri.parse("https://dl.freemp3downloads.online/file/youtubeoyEuk8j8imI128.mp3?fn=Justin%20Bieber%20-%20Love%20Yourself%20%20(PURPOSE%20%3A%20The%20Movement).mp3");
//        ExtractorMediaSource audioSource =
//                new ExtractorMediaSource.Factory(
//                        new DefaultHttpDataSourceFactory("exoplayer-codelab")).
//                        createMediaSource(audioUri);
//
//        Uri audioUri2 = Uri.parse("https://dl.freemp3downloads.online/file/youtubeoyEuk8j8imI128.mp3?fn=Justin%20Bieber%20-%20Love%20Yourself%20%20(PURPOSE%20%3A%20The%20Movement).mp3");
//        ExtractorMediaSource audioSource2 =
//                new ExtractorMediaSource.Factory(
//                        new DefaultHttpDataSourceFactory("exoplayer-codelab")).
//                        createMediaSource(audioUri2);
//
//        return new ConcatenatingMediaSource(audioSource, videoSource);
//    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
//            initializePlayer("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideSystemUi();
        if ((Util.SDK_INT <= 23)) {
//            initializePlayer("/storage/emulated/0/NCT/GiacMo-BuiAnhTuanYanbi-5749598.mp3");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        if (playerViewDialog == null) return;

        playerViewDialog.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void releasePlayer() {
        if (player != null) {
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();
//            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

    private void scanFileInDevice() {
        String[] projectionAudio = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST
        };

        final String sortOrder = MediaStore.Audio.AudioColumns.TITLE + " COLLATE LOCALIZED ASC";
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        List<MediaObject> mediaFiles = new ArrayList<>();

        Cursor cursor = null;
        try {
            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            cursor = getContentResolver().query(uri, projectionAudio, selection, null, sortOrder);
            if( cursor != null) {
                cursor.moveToFirst();

                while(!cursor.isAfterLast()) {
                    String title = cursor.getString(0);
                    String data = cursor.getString(1);
                    String displayName = cursor.getString(2);
                    String artTist  = cursor.getString(3);

                    MediaObject mediaObject = new MediaObject();
                    mediaObject.setTitle(title);
                    mediaObject.setArtTist(artTist);
                    mediaObject.setData(data);
                    mediaObject.setDisplayName(displayName);
                    mediaObject.setAudio(true);

                    mediaFiles.add(mediaObject);

                    cursor.moveToNext();
                }

            }

            String[] projectionVideo = {
                    MediaStore.Video.Media.TITLE,
                    MediaStore.Video.Media.DATA,
                    MediaStore.Video.Media.DISPLAY_NAME,
                    MediaStore.Video.Media.ARTIST
            };

            uri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            cursor = getContentResolver().query(uri, projectionVideo, null, null, sortOrder);
            if( cursor != null) {
                cursor.moveToFirst();

                while(!cursor.isAfterLast()){
                    String title = cursor.getString(0);
                    String data = cursor.getString(1);
                    String displayName = cursor.getString(2);

                    MediaObject mediaObject = new MediaObject();
                    mediaObject.setTitle(title);
                    mediaObject.setData(data);
                    mediaObject.setDisplayName(displayName);

                    mediaFiles.add(mediaObject);

                    cursor.moveToNext();
                }

            }

            mediaAdapter.setData(mediaFiles);

        } catch (Exception e) {
            Log.e("TAG", e.toString());
        } finally {
            if( cursor != null){
                cursor.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSION_READ_EXTERNAL_STORAGE :

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    scanFileInDevice();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                }
                return;
        }
    }

    @Override
    public void onItemClick(int position, MediaObject mediaObject) {
        if (mediaObject == null || TextUtils.isEmpty(mediaObject.getData()))
            return;

        showDialogPlayer(mediaObject.getData());
    }

    private void showDialogPlayer(String path) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_media_player, null);
        playerViewDialog = view.findViewById(R.id.dialog_media_player_view);
        ImageView btnClose = view.findViewById(R.id.dialog_media_btn_close);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                releasePlayer();
                dialogMediaPlayer.dismiss();
            }
        });

        builder.setView(view);
        builder.setCancelable(false);

        dialogMediaPlayer = builder.create();
        dialogMediaPlayer.show();

        initializePlayer(path);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (playerViewDialog != null && playerViewDialog.isShown())
            releasePlayer();
    }
}
